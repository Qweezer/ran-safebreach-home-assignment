/**
 * Created by Ran on 07/10/2015.
 */

var http = require('http');
var redis = require('redis');

var redisClient = redis.createClient();

redisClient.on('error', function(err, reply) {
    console.log('Error from redis server: ' + err);
});

var redisAssetsListKey = "cncServerAssetsList";
var cncServerPort = 5000;
var cncServerUrl = "/cnc/assets";

function handleRequest(request, response)
{
    if(!validateUrl(request.url))
    {
        response.end("Error: invalid url: " + request.url);
        return;
    }

    var data = "";

    request.on('data', function (chunk)
    {
        data += chunk;
    });

    request.on('end', function()
    {
        console.log('Data: ' + data);

        dispatchRequest(request.method, data, response);
    });

    request.on('error', function()
    {
        console.log("Error:" + err);
        response.end("Error:" + err);
    });
}

function validateUrl(url)
{
    var modifiedUrl = url.toString().trim().toLowerCase();

    return modifiedUrl === cncServerUrl;
}


function dispatchRequest(method, body, response)
{
    switch(method) {
        case "POST":
            addNewAsset(body, response);
            break;
        case "GET":
            getAllAssets(response);
            break;
        case "DELETE":
            deleteAllAssets(response);
            break;
        default:
            console.log('Error: Invalid Method ' + method);
            response.end('Error: Invalid Method ' + method);
            break;
    }
}

function addNewAsset(body, response)
{
    redisClient.rpush([redisAssetsListKey, body], function(err, reply)
    {
        response.end("\'{\"NumAssets\": " + reply + "}\'");
    });
}

function getAllAssets(response)
{
    redisClient.lrange(redisAssetsListKey, 0, -1, function(err, reply) {

        console.log(reply);

        var assets = reply;
        var formattedAssets = getFormattedAssets(assets);

        response.end(formattedAssets.toString());
    });
}

function deleteAllAssets(response)
{
    redisClient.lrange(redisAssetsListKey, 0, -1, function(err, reply)
    {
        console.log(reply);

        var assets = reply;
        var formattedAssets = getFormattedAssets(assets);

        redisClient.del(redisAssetsListKey, function(err, reply)
        {
            console.log(reply);
            response.end(formattedAssets.toString());
        });
    });
}

function getFormattedAssets(assets)
{
        // If the data saved in another format, it can be build in here to create the formatted list output
        var formattedAssets = "'[";
        var numberOfAssets = assets.length;

        for(var i = 0; i < numberOfAssets; i++)
        {
            formattedAssets += assets[i];
            if(i != numberOfAssets-1)
            {
                formattedAssets += ",";
            }
        }

        formattedAssets += "]'";

        return formattedAssets;
}

//Creating the server
var server = http.createServer(handleRequest);

function serverLogin()
{
    console.log("Server Up and listening on: http://localhost:%s", cncServerPort);
}

//Starting the server
server.listen(cncServerPort, serverLogin);

// curl -X POST -H "Content-Type: application/json" http://localhost:5000/cnc/assets -d "{"CreditCard": {"IssuingNetwork": American Express, "CardNumber": 375283401434484}}"
// curl -X GET http://localhost:5000/cnc/assets
// curl -X DELETE http://localhost:5000/cnc/assets